barman (2.12-1) unstable; urgency=medium

  * New upstream version 2.12

 -- Marco Nenciarini <mnencia@debian.org>  Wed, 04 Nov 2020 09:39:52 +0000

barman (2.11-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure copyright file specification URI.

  [ Marco Nenciarini ]
  * Mail from /etc/cron.d/barman are now delivered to root by default
    (Closes: #893569)
  * New upstream version 2.11
  * Remove patch no-connection-from-backupinfo
  * Split out barman-cli-cloud from barman-cli
  * Bump Standards Version to 4.5.0 (no changes)
  * Set debhelper compatibility level to 10

 -- Marco Nenciarini <mnencia@debian.org>  Thu, 09 Jul 2020 13:18:47 +0200

barman (2.10-2) unstable; urgency=medium

  * Add upstream patch no-connection-from-backupinfo.
    Prevent Barman from opening a PostgreSQL transaction that lasts until
    termination, even in operations that should not require to speak with
    Postgres (i.e. list-backup or recover).
    This fixes a regression introduced in 2.10 with systemid check code.

 -- Marco Nenciarini <mnencia@debian.org>  Wed, 04 Dec 2019 18:34:59 +0100

barman (2.10-1) unstable; urgency=medium

  * New upstream version 2.10
  * Drop gh-239.patch patch (fixed upstream)
  * Bump Standards Version to 4.4.1 (no changes)
  * Recommend python3-boto3 with barman-cli

 -- Marco Nenciarini <mnencia@debian.org>  Mon, 02 Dec 2019 12:44:04 +0100

barman (2.9-2) unstable; urgency=medium

  * Add patch fixing PostgreSQL connection aliveness check (GH-239)

 -- Marco Nenciarini <mnencia@debian.org>  Mon, 21 Oct 2019 14:44:19 +0200

barman (2.9-1) unstable; urgency=medium

  [ Christoph Berg ]
  * Add debian/gitlab-ci.yml.

  [ Marco Nenciarini ]
  * New upstream version 2.9

 -- Marco Nenciarini <mnencia@debian.org>  Fri, 26 Jul 2019 18:56:45 +0200

barman (2.8-1) unstable; urgency=medium

  * New upstream version 2.8
  * Restructure packages
    - Add python3-barman and barman-cli binaries
    - Build with python3

 -- Marco Nenciarini <mnencia@debian.org>  Tue, 14 May 2019 19:09:42 +0200

barman (2.7-1) unstable; urgency=medium

  * New upstream version 2.7

 -- Marco Nenciarini <mnencia@debian.org>  Wed, 20 Mar 2019 12:12:44 +0100

barman (2.6-1) unstable; urgency=medium

  * New upstream version 2.6
  * Bump Standards Version to 4.3.0 (no changes)

 -- Marco Nenciarini <mnencia@debian.org>  Thu, 31 Jan 2019 19:27:28 +0100

barman (2.5-1) unstable; urgency=medium

  * New upstream version 2.5
  * Install upstream release notes in /usr/share/doc/barman/NEWS.gz
  * Bump Standards Version to 4.2.1 (no changes)
  * Change priority from extra to optional
  * Use HTTPS in debian/watch URI

 -- Marco Nenciarini <mnencia@debian.org>  Tue, 23 Oct 2018 11:12:54 +0200

barman (2.4-1) unstable; urgency=medium

  [ Christoph Berg ]
  * Move packaging repository to salsa.debian.org

  [ Marco Nenciarini ]
  * New upstream version 2.4
  * Fix bash completion

 -- Marco Nenciarini <mnencia@debian.org>  Thu, 24 May 2018 01:17:32 +0200

barman (2.3-2) unstable; urgency=medium

  * Remove postgresql-client alternatives from Recommends field
  * Set debhelper compatibility level to 9
  * Bump Standards Version to 4.1.0.0 (no changes)

 -- Marco Nenciarini <mnencia@debian.org>  Thu, 28 Sep 2017 10:49:00 +0200

barman (2.3-1) unstable; urgency=medium

  * debian/watch: enable cryptographic signature verification
  * New upstream version 2.3

 -- Marco Nenciarini <mnencia@debian.org>  Tue, 05 Sep 2017 12:17:54 +0200

barman (2.2-1) unstable; urgency=medium

  * New upstream version 2.2

 -- Marco Nenciarini <mnencia@debian.org>  Mon, 17 Jul 2017 14:53:26 +0200

barman (2.1-1) unstable; urgency=medium

  * New upstream version 2.1
  * The file README has been renamed to README.rst upstream

 -- Marco Nenciarini <mnencia@debian.org>  Thu, 05 Jan 2017 15:25:27 +0100

barman (2.0-1) unstable; urgency=medium

  * New upstream version 2.0
  * Update package description
  * Create the /etc/barman.d directory
  * Remove barman-wal-restore.py from examples directory
  * Suggests barman-cli package, which contains barman-wal-restore
  * Add repmgr >= 3.2 to suggested packages
  * Recommends all the supported postgresql-client packages
  * Explicitly depends on dh-python

 -- Marco Nenciarini <mnencia@debian.org>  Mon, 26 Sep 2016 19:17:27 +0200

barman (1.6.1-1) unstable; urgency=medium

  * Imported Upstream version 1.6.1
  * Bump Standards-Version to 3.9.8.0 (no changes)

 -- Marco Nenciarini <mnencia@debian.org>  Fri, 20 May 2016 17:52:00 +0200

barman (1.6.0-1) unstable; urgency=medium

  * Imported Upstream version 1.6.0
  * Use HTTPS protocol in Vcs-Browser URI inside debian/control file

 -- Marco Nenciarini <mnencia@debian.org>  Fri, 26 Feb 2016 17:48:00 +0100

barman (1.5.1-1) unstable; urgency=medium

  * Imported Upstream version 1.5.1
  * Install barman-wal-restore script in the example directory

 -- Marco Nenciarini <mnencia@debian.org>  Mon, 16 Nov 2015 12:20:14 +0100

barman (1.5.0-1) unstable; urgency=medium

  * Imported Upstream version 1.5.0

 -- Marco Nenciarini <mnencia@debian.org>  Sat, 26 Sep 2015 17:10:33 +0200

barman (1.4.1-1) unstable; urgency=medium

  * Updated package description
  * debian/watch: update watch file to use the cheeseshop as source.
  * Imported Upstream version 1.4.1

 -- Marco Nenciarini <mnencia@debian.org>  Tue, 05 May 2015 12:04:52 +0200

barman (1.4.0-1) unstable; urgency=medium

  * Imported Upstream version 1.4.0

 -- Marco Nenciarini <mnencia@debian.org>  Fri, 23 Jan 2015 17:08:36 +0100

barman (1.4.0~alpha.1-1) unstable; urgency=medium

  * Update watchfile to mangle pre releases
  * Imported Upstream version 1.4.0~alpha.1
    + Closes: #768296 Missing pg_ident.conf is treated as hard failure
  * Bump Standards-Version to 3.9.6.0 (no changes)

 -- Marco Nenciarini <mnencia@debian.org>  Mon, 12 Jan 2015 16:20:19 +0100

barman (1.3.3-1) unstable; urgency=medium

  * Imported Upstream version 1.3.3

 -- Marco Nenciarini <mnencia@debian.org>  Mon, 18 Aug 2014 16:51:02 +0200

barman (1.3.3~alpha.1-1) experimental; urgency=medium

  * Imported Upstream version 1.3.3~alpha.1

 -- Marco Nenciarini <mnencia@debian.org>  Wed, 23 Jul 2014 18:50:31 +0200

barman (1.3.2-1) unstable; urgency=medium

  * Imported Upstream version 1.3.2

 -- Marco Nenciarini <mnencia@debian.org>  Tue, 15 Apr 2014 18:28:14 +0200

barman (1.3.1-1) unstable; urgency=medium

  * Imported Upstream version 1.3.1

 -- Marco Nenciarini <mnencia@debian.org>  Fri, 11 Apr 2014 12:17:14 +0200

barman (1.3.0-1) unstable; urgency=medium

  * Imported Upstream version 1.3.0
  * Bump Standards-Version to 3.9.5.0 (no changes)

 -- Marco Nenciarini <mnencia@debian.org>  Mon, 03 Feb 2014 23:10:34 +0100

barman (1.2.3-1) unstable; urgency=low

  * Imported Upstream version 1.2.3

 -- Marco Nenciarini <mnencia@debian.org>  Fri, 06 Sep 2013 10:59:02 +0200

barman (1.2.2-1) unstable; urgency=low

  * Imported Upstream version 1.2.2

 -- Marco Nenciarini <mnencia@debian.org>  Tue, 25 Jun 2013 12:11:26 +0200

barman (1.2.1-1) unstable; urgency=low

  * Imported Upstream version 1.2.1
  * Canonicalize VCS-* field URI in debian/control
  * Add argcomplete to debian/pydist-overrides

 -- Marco Nenciarini <mnencia@debian.org>  Tue, 18 Jun 2013 11:49:24 +0200

barman (1.2.0-1) unstable; urgency=low

  * Add support for build on Ubuntu Lucid (10.4)
  * Imported Upstream version 1.2.0

 -- Marco Nenciarini <mnencia@debian.org>  Fri, 08 Feb 2013 17:31:39 +0100

barman (1.1.2-1) unstable; urgency=low

  * Imported Upstream version 1.1.2

 -- Marco Nenciarini <mnencia@debian.org>  Wed, 05 Dec 2012 12:50:54 +0100

barman (1.1.1-1) unstable; urgency=low

  * Imported Upstream version 1.1.1

 -- Marco Nenciarini <mnencia@debian.org>  Fri, 19 Oct 2012 13:21:00 +0200

barman (1.1.0-1) unstable; urgency=low

  * New upstream version 1.1.0

 -- Marco Nenciarini <mnencia@debian.org>  Mon, 15 Oct 2012 10:48:53 +0200

barman (1.0.0-2) unstable; urgency=low

  * Check for existence of barman system user instead of postgres user.
    Thanks to Salvatore Bonaccorso <carnil@debian.org> (Closes: #689924)

 -- Marco Nenciarini <mnencia@debian.org>  Sun, 07 Oct 2012 23:42:38 +0200

barman (1.0.0-1) unstable; urgency=low

  * Initial release (Closes: #683486)

 -- Marco Nenciarini <mnencia@debian.org>  Fri, 05 Oct 2012 16:50:15 +0200
